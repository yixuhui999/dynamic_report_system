# 动态报表系统

#### 项目介绍
spring boot + vue + echart.js 搭建动态BI报表视图

## quartz
继承quartz集群，数据库文件见resource目录中的tables_mysql.sql文件

1.创建job实例工厂，解决spring注入问题，如果使用默认会导致spring的@Autowired 无法注入问题
2.在SchedulerConfiguration.java 配置文件中 将quartz的配置文件读入
