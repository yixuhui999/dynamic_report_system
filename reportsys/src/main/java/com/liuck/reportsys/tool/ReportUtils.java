package com.liuck.reportsys.tool;

import java.io.File;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.liuck.reportsys.quartz.factory.BaseCronTrigger;

public class ReportUtils {

	/**
	 * 判断字符串是否非空
	 * @author liuck
	 * @since  2019年1月1日
	 * @param str
	 * @return true-空  false-非空
	 */
	public static boolean isEmptyStr(String str) {
		if(str == null || "".equals(str)) {
			return true;
		}
		return false;
	}
	
	/**
	 * 判断List集合是否非空
	 * @author liuck
	 * @since  2019年1月1日
	 * @param list
	 * @return true-空  false-非空
	 */
	public static boolean isEmptyList(List<?> list){
		if(list == null || list.size() == 0) {
			return true;
		}
		return false;
	}
	
	/**
	 * 当前系统时间以自定义格式输出
	 * @author liuck
	 * @since  2019年1月1日
	 * @param pattern 日期格式
	 * @return
	 */
	public static String formatDateToStr(String pattern) {
		String dateStr = "";
		if(!isEmptyStr(pattern)) {
			SimpleDateFormat sdf = new SimpleDateFormat(pattern);
			dateStr = sdf.format(new Date());
		}
		return dateStr;
	}
	
	/**
	 * 将字符串日期以自定义格式输出
	 * @author liuck
	 * @since  2019年1月1日
	 * @param dateStr
	 * @param pattern
	 * @return  如果都为空  则将当前系统时间以yyyyMMdd格式输出
	 * @throws ParseException  格式匹配异常
	 */
	public static Date formatStrToDate(String dateStr, String pattern) throws ParseException {
		Date date = new Date();
		if(!isEmptyStr(dateStr) && !isEmptyStr(pattern)) {
			SimpleDateFormat sdf = new SimpleDateFormat(pattern);
			date = sdf.parse(dateStr);
		}else {
			SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd");
			date = sdf.parse(sdf.format(new Date()));
		}
		return date;
	}
	
	/**
	 * 根据包名查询该包名下所有的子文件
	 * 目前只是遍历的class文件，如果后续打包成jar包的话方法另外再写吧  哎  懒得写
	 * @author liuck
	 * @since  2019年1月1日
	 * @param packageName  包名 com.liuck
	 * @return
	 */
	public static List<String> getAllFilePathByPackageName(String packageName){
		String filePath = packageName.replaceAll("[.]", "/");
		String clazzPath = ClassLoader.getSystemClassLoader().getResource(filePath).getPath();
		File file = new File(clazzPath);
		List<String> cronList = new ArrayList<String>();//包下所有继承了CronTriggerBean文件
		if(file.exists() && file.isDirectory()) {
			File[] files = file.listFiles();
			for(File javaFile : files) {
				String javaName = javaFile.getName();
				//包下所有的文件
				String cronPath = packageName+"."+javaName;
				if(javaFile.isDirectory()) {
					//迭代取子目录下的所有文件
					cronList.addAll(getAllFilePathByPackageName(cronPath));
				}else {
					//过滤非.class后缀的文件
					if(!ReportUtils.isEmptyStr(javaName)) {
						if(!javaName.contains(".class")) {
							continue;
						}
					}
					cronPath = cronPath.replace(".class", "");
					try {
						//加载类
						Class<?> clazz = Class.forName(cronPath);
						if(BaseCronTrigger.class.isAssignableFrom(clazz)
								&& !BaseCronTrigger.class.equals(clazz)) {
							//如果该类继承了CronTriggerBean
							cronList.add(cronPath);
						}
					} catch (ClassNotFoundException e) {
						e.printStackTrace();
					}
				}
			}
		}
		return cronList;
	}
}
