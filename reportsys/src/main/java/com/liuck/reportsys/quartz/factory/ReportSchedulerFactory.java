package com.liuck.reportsys.quartz.factory;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.List;

import org.quartz.JobDetail;
import org.quartz.Scheduler;
import org.quartz.SchedulerException;
import org.quartz.Trigger;
import org.quartz.impl.StdSchedulerFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.liuck.reportsys.tool.ReportUtils;

public class ReportSchedulerFactory {
	
	public ReportSchedulerFactory(List<String> cronList) {
		try {
			startJob(cronList);
		} catch (SchedulerException e) {
			e.printStackTrace();
		}
	}
	
	Logger log = LoggerFactory.getLogger(ReportSchedulerFactory.class);
	
	public void startJob(List<String> cronList) throws SchedulerException {
		if(!ReportUtils.isEmptyList(cronList)) {
			Scheduler scheduler = StdSchedulerFactory.getDefaultScheduler();
			scheduler.start();
			Class<?> clazz;
			try {
				for(int i=0;i<cronList.size();i++) {
					clazz = Class.forName(cronList.get(i));
					Method jobDetailMethod = clazz.getDeclaredMethod("getJobDetail");
					Method cronTriggerMethod = clazz.getDeclaredMethod("getCronTrigger");
					Object jobDetailValue = jobDetailMethod.invoke(clazz.newInstance());
					Object cronTriggerValue = cronTriggerMethod.invoke(clazz.newInstance());
					scheduler.scheduleJob((JobDetail)jobDetailValue, (Trigger)cronTriggerValue);
				}
			} catch (ClassNotFoundException e) {
				log.info("找不到类 "+e.getMessage());
				e.printStackTrace();
			} catch (NoSuchMethodException e) {
				log.info("找不到方法 "+e.getMessage());
				e.printStackTrace();
			} catch (SecurityException e) {
				log.info("权限异常 "+e.getMessage());
				e.printStackTrace();
			} catch (IllegalAccessException e) {
				log.info("权限异常 "+e.getMessage());
				e.printStackTrace();
			} catch (IllegalArgumentException e) {
				log.info("参数异常 "+e.getMessage());
				e.printStackTrace();
			} catch (InvocationTargetException e) {
				log.info("解析异常 "+e.getMessage());
				e.printStackTrace();
			} catch (InstantiationException e) {
				log.info("实例化异常 "+e.getMessage());
				e.printStackTrace();
			}
		}
	}
}
