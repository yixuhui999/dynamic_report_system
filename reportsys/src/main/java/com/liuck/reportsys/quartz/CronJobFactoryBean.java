package com.liuck.reportsys.quartz;

import org.quartz.spi.TriggerFiredBundle;
import org.springframework.beans.factory.config.AutowireCapableBeanFactory;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.scheduling.quartz.SpringBeanJobFactory;
import org.springframework.stereotype.Component;

/**
 * 创建job实例工厂，解决spring注入问题，如果使用默认会导致spring的@Autowired 无法注入问题
 * @author liuck
 * @since 2019年1月5日
 */
@Component
public class CronJobFactoryBean extends SpringBeanJobFactory implements ApplicationContextAware{

	private transient AutowireCapableBeanFactory beanFactory;
	
	@Override
    public void setApplicationContext(final ApplicationContext context) {
        beanFactory = context.getAutowireCapableBeanFactory();
    }
	
	@Override
    protected Object createJobInstance(final TriggerFiredBundle bundle) throws Exception {
        final Object job = super.createJobInstance(bundle);
        beanFactory.autowireBean(job);
        return job;
    }

}
