package com.liuck.reportsys.quartz.factory;

import org.quartz.CronTrigger;
import org.quartz.JobDetail;

public class RoleCronTrigger extends BaseCronTrigger{

	public RoleCronTrigger() {
		init();
	}

	@Override
	public String getConExpression() {
		//从0秒开始  每隔5秒执行一次
		return "0/5 * * * * ?";
	}

	@Override
	public String getJobDesc() {
		return "roleJob";
	}

	@Override
	public JobDetail getJobDetail() {
		return super.getJobDetail();
	}

	@Override
	public CronTrigger getCronTrigger() {
		return super.getCronTrigger();
	}

	
}
