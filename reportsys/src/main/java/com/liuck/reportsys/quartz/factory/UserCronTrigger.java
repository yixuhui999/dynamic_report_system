package com.liuck.reportsys.quartz.factory;

import org.quartz.CronTrigger;
import org.quartz.JobDetail;

public class UserCronTrigger extends BaseCronTrigger{

	public UserCronTrigger() {
		init();
	}

	@Override
	public String getConExpression() {
		//从0秒开始  每隔两秒执行一次
		return "0/2 * * * * ?";
	}

	@Override
	public String getJobDesc() {
		return "userJob";
	}

	@Override
	public JobDetail getJobDetail() {
		return super.getJobDetail();
	}

	@Override
	public CronTrigger getCronTrigger() {
		return super.getCronTrigger();
	}

	
}
