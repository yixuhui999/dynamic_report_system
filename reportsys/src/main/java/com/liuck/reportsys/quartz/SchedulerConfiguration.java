package com.liuck.reportsys.quartz;

import java.io.IOException;
import java.util.Properties;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.PropertiesFactoryBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.io.ClassPathResource;
import org.springframework.scheduling.quartz.SchedulerFactoryBean;


@Configuration
public class SchedulerConfiguration {
	
	@Autowired
	private CronJobFactoryBean cronJobFactoryBean;
	
//	@Bean
//	public ReportSchedulerFactory reportSchedulerFactory() {
//		List<String> cronList = ReportUtils.getAllFilePathByPackageName("com.liuck.reportsys.quartz.factory");
//		return new ReportSchedulerFactory(cronList);
//	};
	
	public SchedulerConfiguration(){
		System.out.println("初始化");
	}
	
	@Bean(name="schedulerFactoryBean")
	public SchedulerFactoryBean schedulerFactoryBean(DataSource dataSource) throws IOException {
		SchedulerFactoryBean schedulerFactory = new SchedulerFactoryBean();
		PropertiesFactoryBean propertiesFactory = new PropertiesFactoryBean();
		propertiesFactory.setLocation(new ClassPathResource("application.yml"));
		propertiesFactory.afterPropertiesSet();
		Properties pro = propertiesFactory.getObject();
		schedulerFactory.setOverwriteExistingJobs(true);
		schedulerFactory.setAutoStartup(true);
		schedulerFactory.setQuartzProperties(pro);
		schedulerFactory.setJobFactory(cronJobFactoryBean);
		schedulerFactory.setDataSource(dataSource);//覆盖quartz.properties文件中的数据库配置
		return schedulerFactory;
	}
}
