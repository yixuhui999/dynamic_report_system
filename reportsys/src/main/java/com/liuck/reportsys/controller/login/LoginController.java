package com.liuck.reportsys.controller.login;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
  *  登录controller
 * @author liuck
 * @since 2018年11月28日
 */
@RestController
@RequestMapping("/login")
public class LoginController {
	
	Logger log = LoggerFactory.getLogger(LoginController.class);
	/**
	 * 用户登录验证
	 * @author liuck
	 * @since  2018年11月28日
	 * @param userName
	 * @param pwd
	 * @return
	 */
	@GetMapping("/loginCheck")
	public String loginCheck(String userName, String pwd) {
		System.out.println("用户名："+userName+"    用户密码："+pwd);
		log.info("登录日志：userName="+userName+"  pwd="+pwd);
		return "登录成功";
	}
}
