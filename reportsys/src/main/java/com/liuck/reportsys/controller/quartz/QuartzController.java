package com.liuck.reportsys.controller.quartz;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.liuck.reportsys.impl.QuartzService;

@RestController
@RequestMapping("/quartz/*")
public class QuartzController {
	
	@Autowired
	private QuartzService quartzService;

	@GetMapping("addJob")
	public void addCronJob(String jobName, String jobGroup, String jobDesc) {
		quartzService.addCronJob(jobName, jobGroup, jobDesc);
	}
	
	@GetMapping("pauseJob")
	public void pauseJob(String triggerName, String triggerGroup) {
		quartzService.pauseJob(triggerName, triggerGroup);
	}
	
	@GetMapping("resumeJob")
	public void resumeJob(String triggerName, String triggerGroup) {
		quartzService.resumeJob(triggerName, triggerGroup);
	}
	
	@GetMapping("deleteJob")
	public void deleteJob(String jobName, String jobGroup) {
		quartzService.deleteJob(jobName, jobGroup);
	}
}
