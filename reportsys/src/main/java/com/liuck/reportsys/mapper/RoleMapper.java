package com.liuck.reportsys.mapper;

import java.util.List;

import com.liuck.reportsys.bean.resource.Resource;
import com.liuck.reportsys.bean.role.Role;

public interface RoleMapper {

	/**
	 * 获取所有角色
	 * @author liuck
	 * @since  2018年12月23日
	 * @param userId
	 * @return
	 */
	List<Role> getAllRoles();
	
	/**
	 * 根据ID获取角色信息
	 * @author liuck
	 * @since  2018年12月23日
	 * @param roleId
	 * @return
	 */
	Role getRoleById(Integer roleId);
	
	/**
	 * 获取应用中所有的资源
	 * @author liuck
	 * @since  2018年12月23日
	 * @return
	 */
	List<Resource> getAllResources();
}
