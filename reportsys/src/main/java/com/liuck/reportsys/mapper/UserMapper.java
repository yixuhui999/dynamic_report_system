package com.liuck.reportsys.mapper;

import com.liuck.reportsys.bean.user.User;

public interface UserMapper {

	/**
	 * 根据用户ID查询用户信息
	 * @author liuck
	 * @since  2018年12月23日
	 * @param userId
	 * @return
	 */
	User getUserById(Integer userId);
	
	/**
	 * 根据用户账号查询用户信息
	 * @author liuck
	 * @since  2018年12月23日
	 * @param usercode
	 * @return
	 */
	User getUserByCode(String usercode);
}
