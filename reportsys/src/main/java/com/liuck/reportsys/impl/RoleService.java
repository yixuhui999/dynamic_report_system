package com.liuck.reportsys.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.liuck.reportsys.bean.resource.Resource;
import com.liuck.reportsys.bean.role.Role;
import com.liuck.reportsys.mapper.RoleMapper;

@Service
public class RoleService {
	
	@Autowired
	private RoleMapper roleMapper;

	/**
	 * 根据ID查询角色信息
	 * @author liuck
	 * @since  2018年12月23日
	 * @param roleId
	 * @return
	 */
	public Role getRoleById(Integer roleId) {
		
		return roleMapper.getRoleById(roleId);
	}
	
	/**
	 * 查询应用中所有资源
	 * @author liuck
	 * @since  2018年12月23日
	 * @return
	 */
	public List<Resource> getAllResources(){
		
		return roleMapper.getAllResources();
	}
}
