package com.liuck.reportsys.bean.user;

import java.util.List;

import com.liuck.reportsys.bean.resource.Resource;
import com.liuck.reportsys.bean.role.Role;

public class User {

	private Integer id;
	
	private String name;
	
	private String code;
	
	private String email;
	
	private String fullName;
	
	private String password;
	
	private Integer salt;
	
	private List<Role> userRoles;
	
	private List<Resource> userResources;
	
	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getFullName() {
		return fullName;
	}

	public void setFullName(String fullName) {
		this.fullName = fullName;
	}

	public List<Role> getUserRoles() {
		return userRoles;
	}

	public void setUserRoles(List<Role> userRoles) {
		this.userRoles = userRoles;
	}

	public List<Resource> getUserResources() {
		return userResources;
	}

	public void setUserResources(List<Resource> userResources) {
		this.userResources = userResources;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public Integer getSalt() {
		return salt;
	}

	public void setSalt(Integer salt) {
		this.salt = salt;
	}
	
	
}
