package com.liuck.reportsys;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.List;

import org.quartz.JobDetail;
import org.quartz.Scheduler;
import org.quartz.SchedulerException;
import org.quartz.Trigger;
import org.quartz.impl.StdSchedulerFactory;

import com.liuck.reportsys.quartz.factory.BaseCronTrigger;

import junit.framework.Test;
import junit.framework.TestCase;
import junit.framework.TestSuite;

/**
 * Unit test for simple App.
 */
public class AppTest 
    extends TestCase
{
    /**
     * Create the test case
     *
     * @param testName name of the test case
     */
    public AppTest( String testName )
    {
        super( testName );
    }

    /**
     * @return the suite of tests being tested
     */
    public static Test suite()
    {
        return new TestSuite( AppTest.class );
    }

    /**
     * Rigourous Test :-)
     * @throws SchedulerException 
     * @throws ClassNotFoundException 
     * @throws SecurityException 
     * @throws NoSuchMethodException 
     * @throws InstantiationException 
     * @throws InvocationTargetException 
     * @throws IllegalArgumentException 
     * @throws IllegalAccessException 
     */
    public void testApp() throws SchedulerException, ClassNotFoundException, NoSuchMethodException, 
    	SecurityException, IllegalAccessException, IllegalArgumentException, 
    	InvocationTargetException, InstantiationException
    {
    	Scheduler scheduler = StdSchedulerFactory.getDefaultScheduler();
		scheduler.start();
		List<String> cronList = new ArrayList<String>();
		cronList.add("com.liuck.reportsys.quartz.factory.UserCronTrigger");
		cronList.add("com.liuck.reportsys.quartz.factory.RoleCronTrigger");
		for(int i=0;i<cronList.size();i++) {
			Class<?> clazz = Class.forName(cronList.get(i));
			boolean flag = BaseCronTrigger.class.isAssignableFrom(clazz);
			System.out.println("是否继承了CronTriggerBean:"+flag);
			Method jobDetailMethod = clazz.getDeclaredMethod("getJobDetail");
			Method cronTriggerMethod = clazz.getDeclaredMethod("getCronTrigger");
			Object jobDetailValue = jobDetailMethod.invoke(clazz.newInstance());
			Object cronTriggerValue = cronTriggerMethod.invoke(clazz.newInstance());
			scheduler.scheduleJob((JobDetail)jobDetailValue, (Trigger)cronTriggerValue);
		}
    }
}
